# tira-sanasto

Tämä repository sisältää JSON -muotoisen sanaston tietorakenteiseta ja algoritmeista. 

Tiedosto `sanasto.json` on ladattavissa sovelluksille jotka voivat tarjota sanaston näkyville eri tavoin.

Sanasto on vielä kehityksen alla. Sanaston laatimisesta vastaa opettajaverkosto joka koostuu suomalaisten yliopistojen ja ammattikorkeakoulujen opettajista.

## Lisenssi

MIT?
